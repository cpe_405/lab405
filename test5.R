findnumber <- function(list,num,...)
{
  data <- list[!is.na(list)]
  data <- table(data)
  output <- data.frame(data[names(data)==num])
  colnames(output)[1] <- "frequency"
  output
}

a <- sample(1:100, 500, replace=TRUE)
findnumber(a,41)
